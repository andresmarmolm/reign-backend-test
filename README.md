# Reign Backend Test

To run this API, create an .env file in the reign-server folder with the following values:

- DATABSE_USER
- DATABASE_PASSWORD
- API_URL (https://hn.algolia.com/api/v1/search_by_date?query=nodejs)
- JWT_SECRET
- API_PORT

Then run the command

`docker-compose --env-file ./.env up -d`

This will start the API on the port API_PORT, defaulting to port 3000, it will then pull articles from the API_URL shortly after startup.

After that, you may use the Postman collection and environment files found in the collections folder to interact with the API.
