import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthorizationService {
  constructor(private jwtService: JwtService) {}

  async login(): Promise<{ access_token: string }> {
    const payload = { username: 'Anonymous' };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
