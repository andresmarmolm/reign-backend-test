import { JwtModule } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthorizationService } from './authorization.service';

describe('AuthorizationService', () => {
  let service: AuthorizationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'TESTKEY',
          signOptions: { expiresIn: '1H' },
        }),
      ],
      providers: [AuthorizationService],
    }).compile();

    service = module.get<AuthorizationService>(AuthorizationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return a JWT string', async () => {
    const { access_token } = await service.login();
    expect(access_token).toBeDefined();
    expect(access_token.split('.')).toHaveLength(3); // Must be a valid JWT string
  });
});
