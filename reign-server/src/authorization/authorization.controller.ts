import { Controller, Post } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { AuthorizationService } from './authorization.service';

@Controller('authorization')
export class AuthorizationController {
  constructor(private authorizationService: AuthorizationService) {}

  @Post('login')
  @ApiResponse({
    schema: {
      properties: {
        access_token: {
          type: 'string',
        },
      },
    },
  })
  async login() {
    return await this.authorizationService.login();
  }
}
