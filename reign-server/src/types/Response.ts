import { ApiProperty } from '@nestjs/swagger';

export class Response<T> {
  @ApiProperty()
  message: string;

  data?: T;
}
