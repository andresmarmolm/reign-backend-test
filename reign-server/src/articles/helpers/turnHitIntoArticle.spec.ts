import { APIArticlesDto } from '../dto/APIArticle.dto';
import { turnHitIntoArticle } from './turnHitIntoArticle';
import { Article } from '../entities/article.entity';

describe('turnHitIntoArticleTest', () => {
  it('should successfully turn a hit DTO into an Article object', () => {
    const hit: APIArticlesDto['hits'][number] = {
      created_at: '2021-01-01T22:57:54.000Z',
      title: 'We bypassed bytenode and decompiled Node.js bytecode in Ghidra',
      url: 'https://swarm.ptsecurity.com/how-we-bypassed-bytenode-and-decompiled-node-js-bytecode-in-ghidra/',
      story_title: null,
      story_url: null,
      author: 'azalemeth',
      _tags: ['story', 'author_azalemeth', 'story_28779537'],
      objectID: '28779537',
    };
    const article = turnHitIntoArticle(hit);
    expect(article).toBeInstanceOf(Article);
    expect(article.month).toEqual('january');
  });
});
