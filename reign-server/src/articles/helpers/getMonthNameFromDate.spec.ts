import { getMonthNameFromDate } from './getMonthNameFromDate';

describe('getMonthNameFromDate', () => {
  it('should successfully get the month name from a Date object', () => {
    const dates = new Array(12).fill(0).map((_, i) => new Date(2021, i));
    const names = dates.map(getMonthNameFromDate);
    expect(names).toEqual([
      'january',
      'february',
      'march',
      'april',
      'may',
      'june',
      'july',
      'august',
      'september',
      'october',
      'november',
      'december',
    ]);
  });
});
