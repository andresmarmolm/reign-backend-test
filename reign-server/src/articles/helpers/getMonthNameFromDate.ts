export function getMonthNameFromDate(date: Date) {
  return date.toLocaleString('en-US', { month: 'long' }).toLowerCase();
}
