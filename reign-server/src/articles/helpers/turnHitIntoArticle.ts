import { APIArticlesDto } from '../dto/APIArticle.dto';
import { Article } from '../entities/article.entity';
import { getMonthNameFromDate } from './getMonthNameFromDate';

export function turnHitIntoArticle(
  hit: APIArticlesDto['hits'][number],
): Article {
  const article = new Article();
  article.title = hit.story_title || hit.title;
  article.author = hit.author;
  article.objectId = hit.objectID;
  article.tags = hit._tags;
  article.createdAt = new Date(hit.created_at);
  article.url = hit.story_url || hit.url;
  article.month = getMonthNameFromDate(article.createdAt);
  return article;
}
