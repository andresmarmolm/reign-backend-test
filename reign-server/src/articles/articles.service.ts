import { Injectable } from '@nestjs/common';
import { Article } from './entities/article.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { AxiosResponse } from 'axios';
import { APIArticlesDto } from './dto/APIArticle.dto';
import { turnHitIntoArticle } from './helpers/turnHitIntoArticle';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectRepository(Article)
    private articlesRepository: Repository<Article>,
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async getArticles(options: {
    items: number;
    page: number;
    author: string;
    tags: string[];
    title: string;
    month: string;
  }) {
    const { items, page, author, tags, title, month } = options;
    try {
      const articles = await this.articlesRepository
        .createQueryBuilder('article')
        .where('article.author ILIKE :author', { author: `%${author || ''}%` })
        .andWhere('article.title ILIKE :title', { title: `%${title || ''}%` })
        .andWhere('article.month ILIKE :month', { month: `%${month || ''}%` })
        .andWhere('article.tags @> (:tags)::varchar[]', { tags: tags || [] })
        .take(items)
        .skip(page * items)
        .orderBy('article.createdAt', 'DESC')
        .getMany();
      return articles;
    } catch (e) {
      console.error(e.message);
      throw new Error('Unable to obtain articles');
    }
  }

  async deleteArticle(id: number) {
    try {
      return await this.articlesRepository.softDelete({ id });
    } catch (e) {
      console.error(e);
      throw new Error('Unable to delete article');
    }
  }

  async updateArticles() {
    try {
      const request: AxiosResponse<APIArticlesDto> = await firstValueFrom(
        this.httpService.get(this.configService.get('API_URL')),
      );

      const articles = request.data.hits
        .filter((hit) => {
          return (
            (hit.title !== null || hit.story_title !== null) &&
            (hit.story_url !== null || hit.story_url !== null)
          );
        })
        .map((hit) => {
          return turnHitIntoArticle(hit);
        });
      await this.articlesRepository
        .createQueryBuilder()
        .insert()
        .into(Article)
        .values(articles)
        .orIgnore('objectId')
        .execute();
    } catch (e) {
      console.error(e.message);
      throw e;
    }
  }

  @Timeout(10000)
  async updateArticlesOnStartup() {
    await this.updateArticles();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async updateArticlesEveryHour() {
    await this.updateArticles();
  }
}
