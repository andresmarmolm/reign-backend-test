export class APIArticlesDto {
  hits: {
    created_at: string;
    title: string | null;
    url: string | null;
    author: string;
    story_title: string | null;
    story_url: string | null;
    _tags: string[];
    objectID: string;
  }[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveType: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
