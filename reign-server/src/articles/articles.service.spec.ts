import { HttpService } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { of } from 'rxjs';
import { ArticlesService } from './articles.service';
import { Article } from './entities/article.entity';

const createMockRepo = function () {
  const mockHits = [
    {
      created_at: '2021-10-07T15:04:50.000Z',
      title: null,
      url: null,
      author: 'Retric',
      story_title: 'California is shutting down its last nuclear plant',
      story_url:
        'https://www.cnbc.com/2021/10/02/why-is-california-closing-diablo-canyon-nuclear-plant.html',
      parent_id: 28787194,
      created_at_i: 1633619090,
      _tags: ['comment', 'author_Retric', 'story_28786550'],
      objectID: '28787260',
    },
  ];

  const mockRepo = {
    objs: [],
    createQueryBuilder: jest.fn().mockReturnThis(),
    insert: jest.fn().mockReturnThis(),
    into: jest.fn().mockReturnThis(),
    values: jest.fn().mockImplementation(function (arr: Article[]) {
      this.objs = this.objs.concat(arr);
      return this;
    }),
    orIgnore: jest.fn().mockReturnThis(),

    where: jest.fn().mockReturnThis(),
    andWhere: jest.fn().mockReturnThis(),
    take: jest.fn().mockReturnThis(),
    skip: jest.fn().mockReturnThis(),
    orderBy: jest.fn().mockReturnThis(),
    getMany: jest.fn().mockImplementation(function () {
      return this.objs;
    }),
    execute: jest.fn().mockReturnThis(),
  };

  return { mockRepo, mockHits };
};

describe('ArticlesService', () => {
  let service: ArticlesService;
  let mockRepo, mockHits;
  beforeEach(async () => {
    ({ mockRepo, mockHits } = createMockRepo());
    const mockHttpService = {
      get: jest.fn().mockImplementation(function () {
        return of({
          data: {
            hits: mockHits,
          },
        });
      }),
    };

    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        ArticlesService,
        {
          provide: HttpService,
          useValue: mockHttpService,
        },
        {
          provide: getRepositoryToken(Article),
          useValue: mockRepo,
        },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should update articles', async () => {
    await service.updateArticles();
    expect(mockRepo.insert).toHaveBeenCalled();
    expect(mockRepo.into).toHaveBeenCalled();
    expect(mockRepo.values).toHaveBeenCalled();
    expect(mockRepo.orIgnore).toHaveBeenCalled();
    expect(mockRepo.execute).toHaveBeenCalled();
    expect(
      await service.getArticles({
        items: 5,
        page: 0,
        title: '',
        author: '',
        tags: [],
        month: '',
      }),
    ).toHaveLength(1);
  });

  it('should get articles', async () => {
    await service.updateArticles();
    const articles = await service.getArticles({
      items: 5,
      page: 0,
      title: '',
      author: '',
      tags: [],
      month: '',
    });
    expect(mockRepo.where).toHaveBeenCalled();
    expect(mockRepo.andWhere).toHaveBeenCalled();
    expect(mockRepo.take).toHaveBeenCalledWith(5);
    expect(mockRepo.skip).toHaveBeenCalledWith(0);
    expect(articles).toHaveLength(1);
  });
});
