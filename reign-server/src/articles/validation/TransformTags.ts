import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

@Injectable()
export class TransformTags implements PipeTransform<string, string[]> {
  transform(value: string | string[], metadata: ArgumentMetadata) {
    // Checking for both ways to send array query params
    // https://swagger.io/docs/specification/2-0/describing-parameters/#array
    if (Array.isArray(value)) {
      return value;
    }
    return value?.trim().length > 0 ? value.split(',') : [];
  }
}
