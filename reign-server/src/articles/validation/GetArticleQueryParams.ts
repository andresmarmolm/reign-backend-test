import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsDefined,
  IsInt,
  IsString,
  IsPositive,
  Min,
  Max,
} from 'class-validator';

export class GetArticleQueryParams {
  @ApiProperty({ required: true })
  @IsDefined()
  @IsInt()
  @IsPositive()
  @Max(5)
  items: number;

  @ApiProperty({ required: true })
  @IsDefined()
  @IsInt()
  @Min(0)
  page: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  author: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  tags: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  month: string;
}
