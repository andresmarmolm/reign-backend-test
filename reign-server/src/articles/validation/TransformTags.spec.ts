import { TransformTags } from './TransformTags';

describe('TransformTags', () => {
  it('should successfully transform a string of tags into an array of tags', () => {
    const transformer = new TransformTags();
    const tags = 'story,user_1111';
    expect(transformer.transform(tags, {} as any)).toEqual([
      'story',
      'user_1111',
    ]);
  });
});
