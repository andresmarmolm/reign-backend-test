import { ApiProperty } from '@nestjs/swagger';

export class DeleteArticleParams {
  @ApiProperty()
  id: number;
}
