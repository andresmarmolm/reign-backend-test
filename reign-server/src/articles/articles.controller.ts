import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../authorization/guard/jwt-auth.guard';
import { ArticlesService } from './articles.service';
import { Article } from './entities/article.entity';
import { GetArticleQueryParams } from './validation/GetArticleQueryParams';
import { TransformTags } from './validation/TransformTags';
import { Response } from '../types/Response';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiQuery,
  getSchemaPath,
} from '@nestjs/swagger';
import { DeleteArticleParams } from './validation/DeleteArticleParams';

@Controller('articles')
@ApiExtraModels(Response, Article)
export class ArticlesController {
  constructor(private articleService: ArticlesService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiBearerAuth()
  @ApiOkResponse({
    schema: {
      allOf: [
        { $ref: getSchemaPath(Response) },
        {
          properties: {
            data: {
              type: 'array',
              items: { $ref: getSchemaPath(Article) },
            },
          },
        },
      ],
    },
  })
  @ApiQuery({ name: 'tags', required: false })
  async getArticles(
    @Query() queryParams: GetArticleQueryParams,
    @Query('tags', new TransformTags()) tags: string[],
  ): Promise<Response<Article[]>> {
    const { items, page, author, title, month } = queryParams;
    const articles = await this.articleService.getArticles({
      items: +items,
      page: +page,
      author,
      title,
      tags,
      month,
    });
    return {
      message: 'Articles obtained',
      data: articles,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    schema: {
      properties: {
        message: {
          type: 'string',
        },
      },
    },
  })
  async deleteArticles(
    @Param() params: DeleteArticleParams,
  ): Promise<Response<void>> {
    await this.articleService.deleteArticle(params.id);
    return { message: 'Article deleted' };
  }
}
