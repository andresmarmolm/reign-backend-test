import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Article {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({
    type: 'varchar',
    unique: true,
  })
  objectId: string;

  @ApiProperty()
  @Column('timestamptz')
  createdAt: Date;

  @ApiProperty()
  @Column('varchar')
  title: string;

  @ApiProperty()
  @Column('varchar')
  url: string;

  @ApiProperty()
  @Column('varchar')
  author: string;

  @ApiProperty()
  @Column('varchar', { array: true })
  tags: string[];

  @ApiProperty()
  @Column('varchar')
  month: string;

  @ApiProperty()
  @DeleteDateColumn()
  deletedAt: Date;
}
