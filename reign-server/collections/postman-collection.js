{
	"info": {
		"_postman_id": "8bbbd7ae-cabd-4dcb-a4fb-142c02f9ee46",
		"name": "Reign Backend Test",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "Get Articles",
			"request": {
				"method": "GET",
				"header": [
					{
						"key": "Authorization",
						"value": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkFub255bW91cyIsImlhdCI6MTYzMzY5ODg4OSwiZXhwIjoxNjMzNzAyNDg5fQ.rfY1ZPYnJ4yyvBKYcY2f-zm5wbADQhKN06xtiep2FZ0",
						"type": "text"
					}
				],
				"url": {
					"raw": "http://localhost:3000/articles?items=5&page=0&author&title&tags=&month=",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"articles"
					],
					"query": [
						{
							"key": "items",
							"value": "5"
						},
						{
							"key": "page",
							"value": "0"
						},
						{
							"key": "author",
							"value": null
						},
						{
							"key": "title",
							"value": null
						},
						{
							"key": "tags",
							"value": ""
						},
						{
							"key": "month",
							"value": ""
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "Delete Article",
			"request": {
				"method": "DELETE",
				"header": [
					{
						"key": "Authorization",
						"value": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkFub255bW91cyIsImlhdCI6MTYzMzY5NTE4MCwiZXhwIjoxNjMzNjk4NzgwfQ.8St4ehTyG01WpuotIE-6fnsd-ubvVSFbeqH-1wTj5-o",
						"type": "text"
					}
				],
				"url": {
					"raw": "http://localhost:3000/articles/1",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"articles",
						"1"
					]
				}
			},
			"response": []
		},
		{
			"name": "Login",
			"request": {
				"method": "POST",
				"header": [],
				"url": {
					"raw": "http://localhost:3000/authorization/login",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"authorization",
						"login"
					]
				}
			},
			"response": []
		}
	]
}